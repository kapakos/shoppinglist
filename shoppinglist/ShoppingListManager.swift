//
//  ShoppingList.swift
//  shoppinglist
//
//  Created by Petros Kapakos on 20.03.15.
//  Copyright (c) 2015 Petros Kapakos. All rights reserved.
//

import UIKit

var shoppingListMgr : ShoppingListManager = ShoppingListManager()

struct shoppingItem {
    var name = "Un-named"
    var shoppingListName = "Un-named"
}

struct shoppingList {
    var name = "Un-named"    
}

class ShoppingListManager: NSObject {
    
    var shoppingItems =  [shoppingItem]()
    
    func addItem(name: String, listName: String ){
        shoppingItems.append(shoppingItem(name: name, shoppingListName: listName))
    }
}
