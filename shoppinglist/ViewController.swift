//
//  ViewController.swift
//  shoppinglist
//
//  Created by Petros Kapakos on 18.03.15.
//  Copyright (c) 2015 Petros Kapakos. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet var textItem : UITextField!
    @IBOutlet var tblItems : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Events
    @IBAction func btnAddShoppingItem_Click(sender: UIButton) {
        if(textItem.text == ""){
            return
        }
        shoppingListMgr.addItem(textItem.text, listName: "Default")
        self.view.endEditing(true)
        textItem.text = ""
        tblItems.reloadData()
    }
    
    // UITAbleViewSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingListMgr.shoppingItems.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "test")
        
        cell.textLabel?.text = shoppingListMgr.shoppingItems[indexPath.row].name
        
        return cell
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    // UIText Field Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
    
        textField.resignFirstResponder()
        
        return true
    }

}

